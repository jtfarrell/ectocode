from __future__ import absolute_import

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib import messages
from django.conf import settings

from .forms import ProfileSettingsForm, AccountSettingsForm
from accounts.models import UserProfile

User = get_user_model()

@login_required
def home(request):
    return render(request, 'home.html')


@login_required
def profile_settings(request):

    args = {}
    args.update(csrf(request))

    object = UserProfile.objects.get(user_id=request.user.id)

    if request.method == "POST":
        form = ProfileSettingsForm(request.POST, request.FILES, instance=object)
        args["form"] = form
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Profile settings updated.")
            return HttpResponseRedirect(reverse("profile_settings"))

        else:
            return render(request, 'profile_settings.html', args)
            
    else:
        args["form"] = ProfileSettingsForm(instance=object)

        return render(request, 'profile_settings.html', args)


@login_required
def account_settings(request):

    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        form = AccountSettingsForm(request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, "Account settings updated")
            return HttpResponseRedirect(reverse("account_settings"))

        else:
            args["form"] = form
            return render(request, "account_settings.html", args)

    else:
        form = AccountSettingsForm(request.user)

        args["form"] = form

        return render(request, "account_settings.html", args)


@login_required
def profile(request, user_id):

    query = get_object_or_404(User, id=user_id)
    user_info_query = get_object_or_404(UserProfile, user_id=user_id)

    args = {}
    args.update(csrf(request))
    args["username"] = query
    args["user_info"] = user_info_query

    return render(request, 'profile.html', args)
