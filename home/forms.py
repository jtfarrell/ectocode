from __future__ import absolute_import

from django import forms
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import PasswordChangeForm

from accounts.models import UserProfile

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
import os


def validate_file_extensions(file):
    ext = os.path.splitext(file.name)[1]
    valid_extensions = [".jpg", ".jpeg", ".png", ".gif", ".pdf", ".tif"]
    if not ext in valid_extensions:
        raise ValidationError("File format not supported.")


class ProfileSettingsForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(ProfileSettingsForm, self).__init__(*args, **kwargs)
        self.fields["profile_pic"].validators.append(validate_file_extensions)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = reverse("profile_settings")
        self.helper.add_input(Submit('submit', 'Submit'))


class AccountSettingsForm(PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(AccountSettingsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = reverse("account_settings")
        self.helper.add_input(Submit("submit", "Submit"))
