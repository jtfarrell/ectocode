from __future__ import absolute_import

from django import forms
from django.forms import extras
from django.core.urlresolvers import reverse

from .models import User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from datetime import date


class RegistrationForm(forms.ModelForm):
    """
    Form for registering a new account.
    """
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label="Password Confirmation")

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2',]

    def __init__(self, *args, **kwargs):
        """
        Overriding form attributes.
        """
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = reverse('accounts:register')
        self.helper.add_input(Submit('submit', 'Submit'))
        super(RegistrationForm, self).__init__(*args, **kwargs)


    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        cleaned_data = super(RegistrationForm, self).clean()
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError("Passwords don't match. Please enter both fields again.")
        return self.cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):

    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_action = reverse('accounts:login')
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
        super(LoginForm, self).__init__(*args, **kwargs)
