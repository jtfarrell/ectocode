from __future__ import absolute_import

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings

from ectocode import settings

from time import time


def get_upload_file_name(instance, filename):
    return "imgs/profile_pic/%s_%s" % (str(time()).replace('.','_'), filename)


class UserManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not username:
            raise ValueError('Users must have a username')

        email = self.normalize_email(email)
        user = self.model(
            username=username,
            email=email
            )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username,
                                email,
                                password=password
                                )

        user.is_admin = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser):

    username = models.CharField(max_length=254, unique=True, db_index=True)
    email = models.EmailField(max_length=254, unique=True)
    joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    REQUIRED_FIELDS = ['email']

    USERNAME_FIELD = 'username'

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __unicode__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        # Handle whether the user has a specific permission?"
        return True

    def has_module_perms(self, accounts):
        # Handle whether the user has permissions to view the app `app_label`?"
        return True

    @property
    def is_staff(self):
        # Handle whether the user is a member of staff?"
        return self.is_admin

    def __str__(self):
        return self.username



class UserProfile(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    profile_pic = models.FileField(upload_to=get_upload_file_name, default= settings.STATIC_URL + 'imgs/profile_pic/placeholder-hi.png')
    url = models.URLField(blank=True, null=True)
    bio = models.TextField(blank=True)
    github = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    bitbucket = models.URLField(blank=True, null=True)
    stackoverflow = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.location


@receiver(post_save, sender=User)
def create_userProfile(sender, instance, **kwargs):
    if kwargs.get("created", False):
        UserProfile.objects.get_or_create(user_id=instance.id)


User.profile = property(lambda u: UserProfile.objects.get_or_create(user_id=u.id)[0])
