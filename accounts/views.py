from __future__ import absolute_import

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.contrib import auth, messages
from django.conf import settings

from .forms import RegistrationForm, LoginForm


User = get_user_model()

def register(request):

    if request.user.is_authenticated():
        print "is authenticated"
        return HttpResponseRedirect(reverse('home'))

    else:
        args = {}
        args.update(csrf(request))

        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            args['form'] = form
            if form.is_valid():
                form.save()
                sign_in = auth.authenticate(username=request.POST['username'],
                                            password=request.POST['password1'])
                auth.login(request, sign_in)
                #confirmation_email(request.POST['email'])

                return HttpResponseRedirect(reverse('home'))
            else:
                args['form'] = RegistrationForm()

                return render(request, 'register.html', args)
        else:
            args['form'] = RegistrationForm()

            return render(request, 'register.html', args)


def login(request):

    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    else:
        args = {}
        args.update(csrf(request))

        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    return render(request, 'home.html')
                else:
                    messages.add_message(request, settings.DANGER_MESSAGE, 'Your account has been deactivated.')
                    args['form'] = LoginForm()
                    return render(request, 'login.html', args)
            else:
                messages.add_message(request, settings.DANGER_MESSAGE, 'Username or password invalid.')
                args['form'] = LoginForm()
                return render(request, 'login.html', args)
        else:
            args['form'] = LoginForm()

            return render(request, 'login.html', args)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('accounts:login'))

def confirmation_email(email):

    email_subject = 'EctoCode Team'
    email_body = "Hey! thanks for joining EctoCode!"
    send_mail(email_subject, email_body, 'james.t.farrell91@gmail.com',
             [email], fail_silently=False)

    return True
